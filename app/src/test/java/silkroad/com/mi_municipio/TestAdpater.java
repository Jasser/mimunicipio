package silkroad.com.mi_municipio;



import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.util.FragmentTestUtil;


import java.util.Arrays;
import java.util.List;

import silkroad.com.mi_municipio.activities.AboutBudget;
import silkroad.com.mi_municipio.adapter.CommentAdapter;
import silkroad.com.mi_municipio.adapter.PostAdapter;
import silkroad.com.mi_municipio.fragments.TextFragment;
import silkroad.com.mi_municipio.models.Comment;
import silkroad.com.mi_municipio.models.Post;

import static junit.framework.Assert.assertEquals;
import static com.google.common.truth.Truth.assertThat;

import static org.assertj.android.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
public class TestAdpater {
    private Context context;
    private TextFragment fragment;



    @Before
    public void setUp() throws Exception
    {
        context = RuntimeEnvironment.application;
    }

    @Test
    public void fragmentTextView() {
        TextFragment fragment = TextFragment.newInstance(R.string.concepto_18,
                                                "background_1");
        assertThat(fragment).isNotNull();
    }

    @Test
    public void postsRecycleViewHomeAdapter() {
        List<Post> listPost = Arrays.asList(
                new Post("1", "Juan","Agua",
                        "Detalle ..." , "10",
                        "2", "3")
        );

        PostAdapter adapter = new PostAdapter(context, listPost);

        RecyclerView rvParent = new RecyclerView(context);
        rvParent.setLayoutManager(new LinearLayoutManager(context));

        PostAdapter.ViewHolder viewHolder = adapter.onCreateViewHolder(rvParent, 0);

        adapter.onBindViewHolder(viewHolder, 0);
        assertEquals("Agua", viewHolder.titleName.getText().toString());

    }

    @Test
    public void commmentRecycleViewAdapter() {
        List<Comment> listComment = Arrays.asList(
                new Comment(100,"100", "01", "Detalle ...")
        );

        CommentAdapter adapter = new CommentAdapter(context, listComment);

        RecyclerView rvParent = new RecyclerView(context);
        rvParent.setLayoutManager(new LinearLayoutManager(context));

        CommentAdapter.ViewHolder viewHolder = adapter.onCreateViewHolder(rvParent, 0);

        adapter.onBindViewHolder(viewHolder, 0);
        assertEquals("Detalle ...", viewHolder.titleName.getText().toString());

    }

}