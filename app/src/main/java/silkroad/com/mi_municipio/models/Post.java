package silkroad.com.mi_municipio.models;


import android.os.Parcel;
import android.os.Parcelable;

public class Post implements Parcelable {
    private int id;
    private String usuarioId;
    private String nombreUsuario;
    private String title;
    private String body;
    private String departamentoId;
    private String municipioId;
    private String sectorId;
    private String fecha;

    public Post(String usuarioId, String nombreUsuario, String title, String body, String departamentoId, String municipioId, String sectorId) {
        this.nombreUsuario = nombreUsuario;
        this.usuarioId = usuarioId;
        this.title = title;
        this.body = body;
        this.departamentoId = departamentoId;
        this.municipioId = municipioId;
        this.sectorId = sectorId;
    }

    protected Post(Parcel in) {
        id = in.readInt();
        usuarioId = in.readString();
        nombreUsuario = in.readString();
        title = in.readString();
        body = in.readString();
        departamentoId = in.readString();
        municipioId = in.readString();
        sectorId = in.readString();
        fecha = in.readString();
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(usuarioId);
        dest.writeString(nombreUsuario);
        dest.writeString(title);
        dest.writeString(body);
        dest.writeString(departamentoId);
        dest.writeString(municipioId);
        dest.writeString(sectorId);
        dest.writeString(fecha);
    }

    public String getFecha() { return fecha; }
    public String getUsuarioId() { return usuarioId; }
    public String getNombreUsuario() { return nombreUsuario; }
    public String getDepartamentoId() { return departamentoId; }
    public String getMunicipioId() { return municipioId; }
    public String getSectorId() { return sectorId; }
    public String getTitle() { return title; }
    public String getBody() {
        return body;
    }
    public int getId() {
        return id;
    }
}
