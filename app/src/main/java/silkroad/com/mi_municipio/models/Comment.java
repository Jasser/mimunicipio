package silkroad.com.mi_municipio.models;

public class Comment {
    private int id;
    private int postId;
    private String usuarioId;
    private String nombreUsuario;
    private String body;
    private String fecha;

    public String getFecha() { return fecha; }

    public int getId() {
        return id;
    }

    public int getPostId() {
        return postId;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public String getBody() {
        return body;
    }

    public Comment(int postId, String usuarioId, String nombreUsuario, String body) {
        this.postId = postId;
        this.usuarioId = usuarioId;
        this.nombreUsuario = nombreUsuario;
        this.body = body;
    }
}
