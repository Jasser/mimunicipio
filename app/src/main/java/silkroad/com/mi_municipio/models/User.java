package silkroad.com.mi_municipio.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
    public String email;
    public String username;
    public String uid;
    public String departamento;
    public String municipio;
    public String cedula;


    public User() {
    }

    public User(String email, String uid, String municipio, String departamento, String username, String cedula) {
        this.username = username;
        this.email = email;
        this.uid = uid;
        this.departamento = departamento;
        this.municipio = municipio;
        this.cedula = cedula;
    }

    public String getCedula() { return cedula; }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getUid() {
        return uid;
    }

    public String getDepartamento() {
        return departamento;
    }

    public String getMunicipio() {
        return municipio;
    }
}

