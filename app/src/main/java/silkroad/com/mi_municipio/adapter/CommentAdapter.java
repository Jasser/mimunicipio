package silkroad.com.mi_municipio.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import silkroad.com.mi_municipio.models.Comment;
import silkroad.com.mi_municipio.R;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>  {

    private List<Comment> mComments = new ArrayList<>();


    public CommentAdapter(ArrayList<Comment> comments) {
        this.mComments = comments;
    }

    public CommentAdapter(Context context, List<Comment> listComment) {
        this.mComments = listComment;
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_comment_item, parent,false);
        return new CommentAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentAdapter.ViewHolder holder, final int position) {
        holder.titleName.setText(mComments.get(position).getBody());
        holder.authorComment.setText(mComments.get(position).getNombreUsuario());
        holder.dateComment.setText(mComments.get(position).getFecha());

    }

    public void setComments(List<Comment> comments){
        this.mComments = comments;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return  mComments.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder{

         public TextView titleName;
        TextView authorComment;
        TextView dateComment;

        RelativeLayout parentLayout;

        public  ViewHolder(View itemView){
            super(itemView);
            titleName = itemView.findViewById(R.id.comment_body);
            authorComment = itemView.findViewById(R.id.author_comment);
            dateComment = itemView.findViewById(R.id.date_comment);
            parentLayout = itemView.findViewById(R.id.comment_layout);

        }
    }


}
