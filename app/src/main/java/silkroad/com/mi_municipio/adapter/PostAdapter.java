package silkroad.com.mi_municipio.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import silkroad.com.mi_municipio.models.Post;
import silkroad.com.mi_municipio.R;


public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private List<Post> mPosts = new ArrayList<>();
    OnPostListener mOnPostListener;


    public PostAdapter(ArrayList<Post> posts, OnPostListener onPostListener) {
        this.mPosts = posts;
        this.mOnPostListener = onPostListener;
    }

    public PostAdapter(Context context, List<Post> postList) {
        this.mPosts = postList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent,false);
        ViewHolder holder = new ViewHolder(view, mOnPostListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.titleName.setText(mPosts.get(position).getTitle());
        holder.bodyName.setText(mPosts.get(position).getBody());
        holder.authorName.setText(mPosts.get(position).getNombreUsuario());
        holder.fecha.setText(mPosts.get(position).getFecha());
    }

    public void setPosts(List<Post> posts){
        this.mPosts = posts;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() { return  mPosts.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView titleName;
        TextView bodyName;
        TextView authorName;
        TextView fecha;
        RelativeLayout parentLayout;
        OnPostListener onPostListener;

        public  ViewHolder(View itemView, OnPostListener onPostListener){
            super(itemView);
            titleName = itemView.findViewById(R.id.title_name);
            bodyName = itemView.findViewById(R.id.body_name);
            authorName= itemView.findViewById(R.id.author_name);
            fecha = itemView.findViewById(R.id.fecha_post);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            this.onPostListener = onPostListener;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {

            onPostListener.onPostClick(getAdapterPosition());
        }
    }

    public interface OnPostListener{
        void onPostClick (int position);
    }
}
