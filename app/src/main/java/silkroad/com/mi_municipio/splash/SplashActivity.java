package silkroad.com.mi_municipio.splash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;

import silkroad.com.mi_municipio.activities.DiscussionActivity;
import silkroad.com.mi_municipio.activities.LoginActivity;
import silkroad.com.mi_municipio.utils.Util;

public class SplashActivity extends AppCompatActivity
{
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        pref = getSharedPreferences("Preferences", Context.MODE_PRIVATE);

        Intent intentLogin = new Intent(this, LoginActivity.class);
        Intent intentDiscussion = new Intent(this, DiscussionActivity.class);

        if(!TextUtils.isEmpty(Util.getUserMailPrefs(pref)) && !TextUtils.isEmpty(Util.getUserPassPrefs(pref)))
        {
            startActivity(intentDiscussion);
        }
        else
        {
            startActivity(intentLogin);
        }

        finish();
    }
}
