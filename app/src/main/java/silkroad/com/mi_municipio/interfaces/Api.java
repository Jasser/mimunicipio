package silkroad.com.mi_municipio.interfaces;

import okhttp3.RequestBody;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import silkroad.com.mi_municipio.models.Comment;
import silkroad.com.mi_municipio.models.Post;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

public interface Api {

    @GET("posts")
    Call<List<Post>> getPosts (
            @QueryMap Map<String, String> params
    );

    @GET("comments")
    Call<List<Comment>> getComments (
            @QueryMap Map<String, String> params
    );

    @FormUrlEncoded
    @POST("posts")
    Call<Post> createPost(
            @Field("usuarioId") String  usuarioId,
            @Field("nombreUsuario") String nombreUsuario,
            @Field("title") String title,
            @Field("body") String body,
            @Field("departamentoId") String departamentoId,
            @Field("municipioId") String municipioId,
            @Field("sectorId") String sectorId);


    @FormUrlEncoded
    @POST("comments")
    Call<Comment> createComment(
            @Field("usuarioId") String  usuarioId,
            @Field("nombreUsuario") String nombreUsuario,
            @Field("body") String body,
            @Field("postId") String PostId);

}



