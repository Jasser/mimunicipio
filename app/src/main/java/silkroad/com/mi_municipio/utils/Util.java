package silkroad.com.mi_municipio.utils;
import android.content.SharedPreferences;

public class Util
{
    public static String getUserMailPrefs(SharedPreferences preferences)
    {
        return preferences.getString("email", "");
    }

    public static String getUserPassPrefs(SharedPreferences preferences)
    {
        return preferences.getString("pass", "");
    }

    public static String getUserNamePrefs(SharedPreferences preferences)
    {
        return preferences.getString("name", "");
    }

    public static String getUserUidPrefs(SharedPreferences preferences)
    {
        return preferences.getString("uid", "");
    }

    public static Boolean getUserRemeberPrefs(SharedPreferences preferences)
    {
        return preferences.getBoolean("remember", false);
    }

    public static String getUserDepartamentoPrefs(SharedPreferences preferences)
    {
        return preferences.getString("departamento", "");
    }

    public static String getUserMunicipioPrefs(SharedPreferences preferences)
    {
        return preferences.getString("municipio", "");
    }



}
