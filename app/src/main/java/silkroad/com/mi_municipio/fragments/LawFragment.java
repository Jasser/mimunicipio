package silkroad.com.mi_municipio.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import silkroad.com.mi_municipio.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LawFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LawFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public LawFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LawFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LawFragment newInstance(String param1, String param2) {
        LawFragment fragment = new LawFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_law, container, false);
        TextView link1 = view.findViewById(R.id.txt_ley_1);
        link1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://legislacion.asamblea.gob.ni/Normaweb.nsf/($All)/A9659A4CEC31974B062570A10057805E?OpenDocument"));
                startActivity(intent);
            }
        });

        TextView link2 = view.findViewById(R.id.txt_ley_2);
        link2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://legislacion.asamblea.gob.ni/normaweb.nsf/($All)/364A8F72532A0F7D06257137005AF870?OpenDocument"));
                startActivity(intent);
            }
        });

        TextView link3 = view.findViewById(R.id.txt_ley_3);
        link3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://legislacion.asamblea.gob.ni/normaweb.nsf/d0c69e2c91d9955906256a400077164a/07063f0a644958bc062570a10058114a?OpenDocument"));
                startActivity(intent);
            }
        });

        TextView link4 = view.findViewById(R.id.txt_ley_4);
        link4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://legislacion.asamblea.gob.ni/Normaweb.nsf/($All)/F78CA467F5C96D0306257257005FBADC?OpenDocument"));
                startActivity(intent);
            }
        });
        return view;
    }

}
