package silkroad.com.mi_municipio.fragments;



import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.adapter.CommentAdapter;
import silkroad.com.mi_municipio.interfaces.Api;
import silkroad.com.mi_municipio.models.Comment;
import silkroad.com.mi_municipio.models.Post;
import silkroad.com.mi_municipio.utils.ServiceGenerator;
import silkroad.com.mi_municipio.utils.VerticalSpacingItemDecorator;

import static android.text.style.TtsSpan.ARG_NUMBER;

public class DetailFragment extends Fragment
{
    private final String TAG   = "fragment amigo";
    private TextView details;
    private RecyclerView mRecyclerView;
    //vars
    private Post post;
    private ArrayList<Comment> comments = new ArrayList<>();
    CommentAdapter adapter;
    private ServiceGenerator serviceGenerator = ServiceGenerator.getInstance();
    private ProgressDialog mDialog;


    private String text;
    private int number;

    public static DetailFragment newInstance(int number) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_NUMBER, number);
        fragment.setArguments(args);
        return fragment;
    }

    public DetailFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_detail_fragment, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_comment_view);
        mRecyclerView.setLayoutManager( new LinearLayoutManager(getContext()));
        mDialog = new ProgressDialog(getContext());
        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(10);
        mRecyclerView.addItemDecoration(itemDecorator);
        adapter = new CommentAdapter(comments);
        mRecyclerView.setAdapter(adapter);


        if (getArguments() != null) {
            number = getArguments().getInt(ARG_NUMBER);
            getComments(number);
        }

        return view;
    }

    private void getComments(final int postid){
        mDialog.setMessage("Cargando");
        mDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://8.9.11.231:3001/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);

        Map<String, String> params = new HashMap<String, String>();
        params.put("postId", String.valueOf(postid));

        Call<List<Comment>> call = api.getComments(params);
        call.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                if(response.body() == null){
                    adapter.setComments(new ArrayList<Comment>());
                }
                else{
                    comments =  (ArrayList<Comment>) response.body();
                    adapter.setComments(comments);
                }
                mDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                adapter.setComments(new ArrayList<Comment>());
                mDialog.dismiss();
            }
        });

    }


    public void renderText(String text)
    {
        details.setText(text);
    }
}
