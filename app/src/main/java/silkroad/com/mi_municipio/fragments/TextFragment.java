package silkroad.com.mi_municipio.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.Nullable;;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;;
import android.widget.TextView;

import silkroad.com.mi_municipio.R;

import static android.text.style.TtsSpan.ARG_TEXT;
import static android.text.style.TtsSpan.ARG_NUMBER;

public class TextFragment extends Fragment {


    private TextView textoView;

    public TextView getTextoView() {
        return textoView;
    }


    public static TextFragment newInstance(int text, String background) {
        TextFragment fragment = new TextFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TEXT, String.valueOf(text));
        args.putString(ARG_NUMBER, background);
        fragment.setArguments(args);
        return fragment;
    }
    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_text, container, false);
        String nameBackground = "background_app_1";
        textoView = view.findViewById(R.id.text_about_budget);

        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        } else {

            if (getArguments() != null) {
                textoView.setText((Integer.valueOf(getArguments().getString(ARG_TEXT))));
                nameBackground = getArguments().getString(ARG_NUMBER);
            }

            int id_view =  getResources().getIdentifier(
                    nameBackground,
                    "drawable",
                    getContext().getPackageName()
            );
            view.findViewById(R.id.relative_fragment_text).setBackgroundResource(id_view);
        }


        return  view;
    }
}