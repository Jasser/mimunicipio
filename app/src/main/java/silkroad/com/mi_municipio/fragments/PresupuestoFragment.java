package silkroad.com.mi_municipio.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.activities.PostActivity;
import silkroad.com.mi_municipio.activities.WebViewActivity;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PresupuestoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PresupuestoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Spinner spinnerTipoConsulta;
    private Spinner spinnerPeriodo;
    private Button btnConsultar;


    public PresupuestoFragment() {
    }

    public static PresupuestoFragment newInstance(String param1, String param2) {
        PresupuestoFragment fragment = new PresupuestoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_presupuesto_filter, container, false);

        spinnerPeriodo = view.findViewById(R.id.spn_periodo);
        spinnerTipoConsulta = view.findViewById(R.id.spn_tipo_consulta);
        btnConsultar = view.findViewById(R.id.btn_consultar);

        ArrayAdapter<CharSequence> adapterP = ArrayAdapter.createFromResource(getActivity(), R.array.periodos, android.R.layout.simple_spinner_item);
        adapterP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPeriodo.setAdapter(adapterP);

        ArrayAdapter<CharSequence> adapterT = ArrayAdapter.createFromResource(getActivity(), R.array.tipo_consultas, android.R.layout.simple_spinner_item);
        adapterT.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoConsulta.setAdapter(adapterT);

        btnConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String periodo = spinnerPeriodo.getSelectedItem().toString();
                final String tipo_consulta = spinnerTipoConsulta.getSelectedItem().toString();
                final String dominio = (Integer.parseInt(periodo) < 2018) ? "www" : "sigaf";
                final String t_c = "Presupuesto Municipal".equals(tipo_consulta) ? "PM" : "PIA";

                final String url = "http://"+ ""+dominio+".transmuni.gob.ni/cgi-bin/" +
                                    ""+t_c+"_PresupActualizado.cgi?ejercicio=" +
                                    ""+periodo+"&Consulta_Publica=S";

                Intent explicitIntent;
                explicitIntent = new Intent(getActivity(), WebViewActivity.class);
                explicitIntent.putExtra("url", url);
                startActivity(explicitIntent);
            }
        });
        return  view;
    }

}
