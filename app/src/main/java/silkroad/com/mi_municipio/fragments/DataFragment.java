package silkroad.com.mi_municipio.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import silkroad.com.mi_municipio.activities.PostActivity;
import silkroad.com.mi_municipio.activities.PublishActivity;
import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.utils.Util;

public class DataFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener
{

    private Spinner spinnerDepartamentos;
    private Spinner spinnerMunicipios;
    private Spinner spinnerSectores;
    private Button btnBuscar;
    private Button btnPublicar;
    private Button btnMyPosts;
    private DataListener callback;
    private SharedPreferences pref;



    public DataFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        try
        {
            callback = (DataListener) context;
        }
        catch(Exception e)
        {
            throw new ClassCastException(context.toString() + " should implement DataListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.activity_data_fragment, container, false);

        spinnerDepartamentos = view.findViewById(R.id.spnDepartamentos);
        spinnerMunicipios = view.findViewById(R.id.spnMunicipios);
        spinnerSectores = view.findViewById(R.id.spnSector);
        btnBuscar = view.findViewById(R.id.btnBuscar);
        btnPublicar = view.findViewById(R.id.btnPublicar);
        btnMyPosts = view.findViewById(R.id.btnPublicaciones);
        pref = this.getActivity().  getSharedPreferences("Preferences", Context.MODE_PRIVATE);



        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.Departamentos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDepartamentos.setAdapter(adapter);
        spinnerDepartamentos.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapterS = ArrayAdapter.createFromResource(getActivity(), R.array.Sector, android.R.layout.simple_spinner_item);
        adapterS.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSectores.setAdapter(adapterS);

        btnBuscar.setOnClickListener(this);
        btnPublicar.setOnClickListener(this);
        btnMyPosts.setOnClickListener(this);

        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        int[] localidades = {R.array.Todos, R.array.Chinandega, R.array.León, R.array.Managua, R.array.Masaya, R.array.Granada, R.array.Carazo, R.array.Rivas,
                R.array.Nueva_Segovia, R.array.Jinotega, R.array.Madriz, R.array.Esteli, R.array.Matagalpa, R.array.Boaco, R.array.Chontales, R.array.Rio_San_Juan,
                R.array.RAAN, R.array.RAAS};

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), localidades[i], android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMunicipios.setAdapter(adapter);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View v)
    {
        Intent explicitIntent;
        String mun = "";


        final String dep = spinnerDepartamentos.getSelectedItem().toString();
        final String sec = spinnerSectores.getSelectedItem().toString();
        String uid  = Util.getUserUidPrefs(pref);

        if(spinnerMunicipios.getSelectedItem() != null){
            mun = spinnerMunicipios.getSelectedItem().toString();
        }


        switch (v.getId())
        {
            case R.id.btnBuscar:
                explicitIntent = new Intent(getActivity(), PostActivity.class);
                explicitIntent.putExtra("dep", dep);
                explicitIntent.putExtra("mun", mun);
                explicitIntent.putExtra("sec", sec);
                explicitIntent.putExtra("uid", "");
                startActivity(explicitIntent);
            break;

            case R.id.btnPublicaciones:

                explicitIntent = new Intent(getActivity(), PostActivity.class);
                explicitIntent.putExtra("dep", "");
                explicitIntent.putExtra("mun", "");
                explicitIntent.putExtra("sec", "");
                explicitIntent.putExtra("uid", uid);
                startActivity(explicitIntent);
                break;

            case R.id.btnPublicar:
                explicitIntent = new Intent(getActivity(), PublishActivity.class);
                startActivity(explicitIntent);
            break;
        }
    }

    public interface DataListener
    {
        void sendData(String text);
    }
}
