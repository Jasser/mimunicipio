package silkroad.com.mi_municipio.activities;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.fragments.LawFragment;
import silkroad.com.mi_municipio.fragments.PresupuestoFragment;
import silkroad.com.mi_municipio.fragments.TextFragment;

public class AboutBudget extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_budget);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.navview);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    TextFragment.newInstance(R.string.concepto_1,"background_app_1")).commit();
            navigationView.setCheckedItem(R.id.what_municipality);
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int texto = R.string.concepto_1;
        String background = "background_app_1";
        switch (item.getItemId()) {
            case R.id.what_municipality:
                texto = R.string.concepto_1;
                background = "background_app_1";
                break;
            case R.id.what_town_council:
                texto = R.string.concepto_2;
                background = "background_app_2";
                break;
            case R.id.what_municipal_budget:
                texto = R.string.concepto_3;
                background = "background_app_3";
                break;
            case R.id.budget_cycle:
                texto = R.string.concepto_4;
                background = "background_app_4";
                break;
            case R.id.formulation:
                texto = R.string.concepto_5;
                background = "background_app_5";
                break;
            case R.id.discussion_aprobation:
                texto = R.string.concepto_6;
                background = "background_app_6";
                break;
            case R.id.execution:
                texto = R.string.concepto_7;
                background = "background_app_7";
                break;
            case R.id.close_evaluation:
                texto = R.string.concepto_8;
                background = "background_app_8";
                break;
            case R.id.menu_item_9:
                texto = R.string.concepto_9;
                background = "background_app_9";
                break;
            case R.id.menu_item_10:
                texto = R.string.concepto_10;
                background = "background_app_10";
                break;
            case R.id.menu_item_11:
                texto = R.string.concepto_11;
                background = "background_app_11";
                break;
            case R.id.menu_item_12:
                texto = R.string.concepto_12;
                background = "background_app_12";
                break;
            case R.id.menu_item_13:
                texto = R.string.concepto_13;
                background = "background_app_13";
                break;
            case R.id.menu_item_14:
                texto = R.string.concepto_14;
                background = "background_app_14";
                break;
            case R.id.menu_item_15:
                texto = R.string.concepto_15;
                background = "background_app_15";
                break;
            case R.id.menu_item_16:
                texto = R.string.concepto_16;
                background = "background_app_16";
                break;
            case R.id.menu_item_17:
                texto = R.string.concepto_17;
                background = "background_app_17";
                break;
            case R.id.menu_item_18:
                texto = R.string.concepto_18;
                background = "background_app_18";
                break;
            case R.id.know_your_budget:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new PresupuestoFragment()).commit();

                drawer.closeDrawer(GravityCompat.START);
                return true;
            case R.id.lawn:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new LawFragment()).commit();

                drawer.closeDrawer(GravityCompat.START);
                return true;
            case R.id.regresar:
                break;

        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                 TextFragment.newInstance(texto,background)).commit();

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
