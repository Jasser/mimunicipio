package silkroad.com.mi_municipio.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import silkroad.com.mi_municipio.fragments.DataFragment;
import silkroad.com.mi_municipio.fragments.DetailFragment;
import silkroad.com.mi_municipio.R;

public class DiscussionActivity extends FragmentActivity implements DataFragment.DataListener
{
    private boolean isMultiPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion);
    }

    @Override
    public void sendData(String text)
    {
        if(isMultiPanel)
        {
            DetailFragment detailsFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.detailsFragment);
            detailsFragment.renderText(text);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Hubo un problema...", Toast.LENGTH_SHORT).show();
        }
    }

    private void setMultiPanel()
    {
        isMultiPanel = (getSupportFragmentManager().findFragmentById(R.id.detailsFragment) != null);
    }
}
