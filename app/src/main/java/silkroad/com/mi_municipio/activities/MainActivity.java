package silkroad.com.mi_municipio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

import silkroad.com.mi_municipio.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    private Button btnSPM;
    private Button btnAD;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindUI();

        btnSPM.setOnClickListener(this);
        btnAD.setOnClickListener(this);
    }

    private void bindUI()
    {
        btnSPM = findViewById(R.id.btnBudget);
        btnAD = findViewById(R.id.btnDiscussion);
    }

    @Override
    public void onClick(View v)
    {
        Intent explicitIntent;

        switch (v.getId())
        {
            case R.id.btnBudget:
                explicitIntent = new Intent(this, AboutBudget.class);
                startActivity(explicitIntent);
            break;

            case R.id.btnDiscussion:
                explicitIntent = new Intent(this, LoginActivity.class);
                startActivity(explicitIntent);
            break;
        }
    }
}
