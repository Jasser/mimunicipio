package silkroad.com.mi_municipio.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import silkroad.com.mi_municipio.adapter.PostAdapter;
import silkroad.com.mi_municipio.interfaces.Api;
import silkroad.com.mi_municipio.models.Post;
import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.utils.ServiceGenerator;
import silkroad.com.mi_municipio.utils.VerticalSpacingItemDecorator;

public class PostActivity extends AppCompatActivity implements PostAdapter.OnPostListener {
    private static final String TAG = "PostActivity";
    // UI components
    private RecyclerView mRecyclerView;
    //vars
    private ArrayList<Post> posts = new ArrayList<>();
    private PostAdapter postAdapter;
    private ServiceGenerator serviceGenerator = ServiceGenerator.getInstance();
    private ProgressDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        mDialog = new ProgressDialog(this);
        mRecyclerView = findViewById(R.id.recycler_view);
        InitRecyclerView();
        getPosts();
    }

    private void InitRecyclerView (){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(10);
        mRecyclerView.addItemDecoration(itemDecorator);
        postAdapter = new PostAdapter(posts, this);
        mRecyclerView.setAdapter(postAdapter);
    }

    private void getPosts2(){
        mDialog.setMessage("Procesando...");
        mDialog.show();

        String dep  = getIntent().getExtras().getString("dep");
        String mun  = getIntent().getExtras().getString("mun");
        String sec  = getIntent().getExtras().getString("sec");
        String uid  = getIntent().getExtras().getString("uid");


        Map<String, String> params = new HashMap<String, String>();
        params.put("departamentoId", String.valueOf(dep));
        params.put("municipioId", String.valueOf(mun));
        params.put("sectorId", String.valueOf(sec));
        params.put("usuarioId", String.valueOf(uid));

        serviceGenerator.getApi().getPosts(params)
                .enqueue(new Callback<List<Post>>() {
                    @Override
                    public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                        Log.d(TAG, "onResponse: " + response.body());
                        if(response.raw().networkResponse() != null){
                            Log.d(TAG, "onResponse: response is from NETWORK...");
                        }
                        else if(response.raw().cacheResponse() != null
                                && response.raw().networkResponse() == null){
                            Log.d(TAG, "onResponse: response is from CACHE...");
                        }

                        if(response.body() == null){
                            postAdapter.setPosts(new ArrayList<Post>());
                        }
                        else{
                            posts = (ArrayList<Post>) response.body();
                            postAdapter.setPosts(response.body());
                        }
                        mDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<List<Post>> call, Throwable t) {
                        Log.e(TAG, "onFailure: ", t);
                        postAdapter.setPosts(new ArrayList<Post>());
                        mDialog.dismiss();
                    }
                });

    }

    private void getPosts(){
        mDialog.setMessage("Cargando");
        mDialog.show();

        String dep  = getIntent().getExtras().getString("dep");
        String mun  = getIntent().getExtras().getString("mun");
        String sec  = getIntent().getExtras().getString("sec");
        String uid  = getIntent().getExtras().getString("uid");


        Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://8.9.11.231:3001/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        Api api = retrofit.create(Api.class);

        Map<String, String> params = new HashMap<String, String>();
        params.put("departamentoId", String.valueOf(dep));
        params.put("municipioId", String.valueOf(mun));
        params.put("sectorId", String.valueOf(sec));
        params.put("usuarioId", String.valueOf(uid));


        Call<List<Post>> call = api.getPosts(params);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    if(response.body() == null){
                        postAdapter.setPosts(new ArrayList<Post>());
                    }
                    else{
                        posts = (ArrayList<Post>) response.body();
                        postAdapter.setPosts(response.body());
                    }
                }
                mDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                postAdapter.setPosts(new ArrayList<Post>());
                mDialog.dismiss();
            }
        });
    }

    @Override
    public void onPostClick(int position) {
        posts.get(position);
        Intent intent = new Intent(this, PostDetailActivity.class);

        intent.putExtra("selected_post", posts.get(position));
        startActivity(intent);
    }
}
