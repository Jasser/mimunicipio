package silkroad.com.mi_municipio.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.models.User;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener
{
    private EditText txtEmail;
    private EditText txtPassword;
    private EditText textUserName;
    private EditText textCedula;
    private Spinner spnDepartamentos;
    private Spinner spnMunicipios;
    private Button btnRegister;
    private DatabaseReference mDatabase;

    //Firebase
    private FirebaseAuth mAuth;
    private ProgressDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDialog = new ProgressDialog(this);
        spnDepartamentos = findViewById(R.id.spnDepartamentosP);
        spnMunicipios = findViewById(R.id.spnMunicipiosP);
        txtEmail = findViewById(R.id.emailRegister);
        txtPassword = findViewById(R.id.passwordRegister);
        textUserName = findViewById(R.id.nameRegister);
        textCedula = findViewById(R.id.cedula_register);

        ArrayAdapter<CharSequence> adapterP1 = ArrayAdapter.createFromResource(this, R.array.Departamentos_V2, android.R.layout.simple_spinner_item);
        adapterP1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnDepartamentos.setAdapter(adapterP1);
        spnDepartamentos.setOnItemSelectedListener(this);

        btnRegister = findViewById(R.id.buttonRegisterAccount);

        btnRegister.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String email = txtEmail.getText().toString().trim();
                String password = txtPassword.getText().toString().trim();
                final String userName = textUserName.getText().toString().trim();
                final String departamento = spnDepartamentos.getSelectedItem().toString();
                final String municipio = spnMunicipios.getSelectedItem().toString();
                final String cedula = textCedula.getText().toString().trim();


                if(TextUtils.isEmpty(cedula))
                {
                    txtEmail.setError("Campo requerido");
                    return;
                }

                if(TextUtils.isEmpty(userName))
                {
                    txtEmail.setError("Campo requerido");
                    return;
                }

                if(TextUtils.isEmpty(email))
                {
                    txtEmail.setError("Campo requerido");
                    return;
                }

                if(TextUtils.isEmpty(password))
                {
                    txtPassword.setError("Campo requerido");
                    return;
                }

                mDialog.setMessage("Procesando...");
                mDialog.show();

                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if(task.isSuccessful())
                        {
                            mDialog.dismiss();
                            FirebaseUser user = mAuth.getInstance().getCurrentUser();

                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(userName)
                                    .build();

                            user.updateProfile(profileUpdates)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d("Aqui voy perro", "Usuario profile updated.");
                                            }
                                        }
                                    });

                            onAuthSuccess(userName, user.getEmail(), user.getUid(),departamento, municipio, cedula);

                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            Toast.makeText(getApplicationContext(), "Registro completo", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Hubo un problema...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    private void onAuthSuccess(String username, String email, String uid, String departamento, String municipio, String cedula) {
        writeNewUser( username,  email,  uid,  departamento,  municipio, cedula);
    }
    private void writeNewUser(String username, String email, String uid, String departamento, String municipio, String cedula) {
        User user = new User(username,email,uid, departamento, municipio, cedula);
        mDatabase.child("users").child(uid).setValue(user);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        int[] localidades = {R.array.Chinandega_V2, R.array.León_V2, R.array.Managua_V2, R.array.Masaya_V2, R.array.Granada_V2, R.array.Carazo_V2, R.array.Rivas_V2,
                R.array.Nueva_Segovia_V2, R.array.Jinotega_V2, R.array.Madriz_V2, R.array.Esteli_V2, R.array.Matagalpa_V2, R.array.Boaco_V2, R.array.Chontales_V2, R.array.Rio_San_Juan_V2,
                R.array.RAAN_V2, R.array.RAAS_V2};

        ArrayAdapter<CharSequence> adapterP3 = ArrayAdapter.createFromResource(this, localidades[i], android.R.layout.simple_spinner_item);
        adapterP3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnMunicipios.setAdapter(adapterP3);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
