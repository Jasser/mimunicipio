package silkroad.com.mi_municipio.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import silkroad.com.mi_municipio.fragments.DetailFragment;
import silkroad.com.mi_municipio.interfaces.Api;
import silkroad.com.mi_municipio.models.Comment;
import silkroad.com.mi_municipio.models.Post;
import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.utils.Util;

public class PostDetailActivity extends AppCompatActivity {

    private SharedPreferences pref;
    String comment;
    int postId;
    Button submitButton;
    EditText commentInput;
    private ProgressDialog mDialog;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_item);
        pref = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        mDialog = new ProgressDialog(this);
        getIncomingIntent();

        submitButton = (Button) findViewById(R.id.submitButton);
        commentInput = (EditText) findViewById(R.id.nameInput);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String body = commentInput.getText().toString().trim();
                String name  = Util.getUserNamePrefs(pref);
                String uid  = Util.getUserUidPrefs(pref);


                if(TextUtils.isEmpty(body))
                {
                    commentInput.setError("Campo requerido");
                    return;
                }

                commentInput.setText("");
                Comment comment = new Comment(postId,uid, name, body);
                createComment(comment);
            }
        });
    }


    private void createComment(Comment comment){
        mDialog.setMessage("Enviando");
        mDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://8.9.11.231:3001/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<Comment> call = api.createComment(
                comment.getUsuarioId(),
                comment.getNombreUsuario(),
                comment.getBody(),
                String.valueOf(comment.getPostId())

        );

        call.enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {

                if(response.isSuccessful()){
                    if(getIntent().hasExtra("selected_post"))
                    {
                        Post post = getIntent().getParcelableExtra("selected_post");
                        DetailFragment detailsFragment = DetailFragment.newInstance(post.getId());
                        getSupportFragmentManager().beginTransaction().replace(R.id.container_comments, detailsFragment).commit();
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"Hubo un problema", Toast.LENGTH_SHORT).show();
                }

                mDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {
                mDialog.dismiss();
            }
        });
    }

    public void getIncomingIntent(){
        if(getIntent().hasExtra("selected_post"))
        {
            Post post = getIntent().getParcelableExtra("selected_post");
            postId = post.getId();
            DetailFragment detailsFragment = DetailFragment.newInstance(post.getId());
            getSupportFragmentManager().beginTransaction().replace(R.id.container_comments, detailsFragment).commit();
            setDetail(post.getBody());
        }
    }

    public void setDetail(String Detail){
        TextView name = findViewById(R.id.title_detail);
        name.setText(Detail);
    }
}
