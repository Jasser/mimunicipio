package silkroad.com.mi_municipio.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.models.User;
import silkroad.com.mi_municipio.utils.Util;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{
    private SharedPreferences pref;

    private Button btnRegister;
    private Button btnLogin;
    private EditText txtEmail;
    private EditText txtPassword;
    private SwitchCompat switchRememberPref;

    private FirebaseAuth mAuth;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        bindUI();

        pref = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        setCredentialsIfExist();

        btnRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    private void bindUI()
    {
        btnRegister = findViewById(R.id.buttonRegister);
        btnLogin = findViewById(R.id.buttonLogin);
        txtEmail = findViewById(R.id.emailLogin);
        txtPassword = findViewById(R.id.passwordLogin);
        switchRememberPref = findViewById(R.id.switchRemember);

        mAuth = FirebaseAuth.getInstance();
        mDialog = new ProgressDialog(this);
    }

    @Override
    public void onClick(View v)
    {
        Intent explicitIntent;

        switch (v.getId())
        {
            case R.id.buttonLogin:
                final String mEmail = txtEmail.getText().toString().trim();
                final String mPassword = txtPassword.getText().toString().trim();

                if(TextUtils.isEmpty(mEmail))
                {
                    txtEmail.setError("Campo requerido");
                    return;
                }

                if(TextUtils.isEmpty(mPassword))
                {
                    txtPassword.setError("Campo requerido");
                    return;
                }

                mDialog.setMessage("Procesando...");
                mDialog.show();

                mAuth.signInWithEmailAndPassword(mEmail, mPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if(task.isSuccessful())
                        {
                            mDialog.dismiss();
                            final FirebaseUser user = mAuth.getInstance().getCurrentUser();

                            DatabaseReference ref =  FirebaseDatabase.getInstance().getReference();
                            Query myMostViewedPostsQuery = ref.child("users").child(user.getUid());
                            // Attach a listener to read the data at our posts reference
                            myMostViewedPostsQuery.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    startActivity(new Intent(getApplicationContext(), DiscussionActivity.class));
                                    Toast.makeText(getApplicationContext(), "Sesión iniciada", Toast.LENGTH_SHORT).show();
                                    User usermodel = dataSnapshot.getValue(User.class);
                                    saveOnPreferences(mEmail, mPassword, user.getDisplayName(), user.getUid(), usermodel.getMunicipio(),usermodel.getDepartamento());
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });


                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Hubo un problema...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            break;

            case R.id.buttonRegister:
                explicitIntent = new Intent(this, RegisterActivity.class);
                startActivity(explicitIntent);
            break;
        }

    }

    private void saveOnPreferences(String email, String password, String userName, String uid, String municipio, String departamento)
    {
        SharedPreferences.Editor editor = pref.edit();

        if(switchRememberPref.isChecked())
        {
            editor.putString("email", email);
            editor.putString("pass", password);
            editor.putBoolean("remember", true);
            editor.putString("name", userName);
            editor.putString("uid", uid);
            editor.putString("departamento", departamento);
            editor.putString("municipio", municipio);

        }else
        {
            editor.putString("email", "");
            editor.putString("pass", "");
            editor.putBoolean("remember", false);
            editor.putString("name", "");
            editor.putString("uid", "");
            editor.putString("departamento", "");
            editor.putString("municipio", "");
        }
        editor.commit();
        editor.apply();
    }

    private void setCredentialsIfExist()
    {
        String email = Util.getUserMailPrefs(pref);
        String password = Util.getUserPassPrefs(pref);
        Boolean remember = Util.getUserRemeberPrefs(pref);
        String name  = Util.getUserNamePrefs(pref);
        String uid  = Util.getUserUidPrefs(pref);


        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password))
        {
            txtEmail.setText(email);
            txtPassword.setText(password);
            switchRememberPref.setChecked(remember);
        }
    }
}


