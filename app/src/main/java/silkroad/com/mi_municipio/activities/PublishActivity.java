package silkroad.com.mi_municipio.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import silkroad.com.mi_municipio.R;
import silkroad.com.mi_municipio.interfaces.Api;
import silkroad.com.mi_municipio.models.Post;
import silkroad.com.mi_municipio.utils.Util;

public class PublishActivity extends AppCompatActivity
{
    private SharedPreferences pref;
    private TextView txtvTitulo;
    private TextView txtvCuerpo;
    private Spinner spnSectores;
    private Button btnPublicar;
    private ProgressDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish);
        pref = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        mDialog = new ProgressDialog(this);
        bindUI();

        ArrayAdapter<CharSequence> adapterP2 = ArrayAdapter.createFromResource(this, R.array.Sector_V2, android.R.layout.simple_spinner_item);
        adapterP2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSectores.setAdapter(adapterP2);

        btnPublicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = txtvTitulo.getText().toString().trim();
                String body = txtvCuerpo.getText().toString().trim();
                String departamento = Util.getUserDepartamentoPrefs(pref);
                String municipio = Util.getUserMunicipioPrefs(pref);
                String sector = spnSectores.getSelectedItem().toString();
                String name  = Util.getUserNamePrefs(pref);
                String uid  = Util.getUserUidPrefs(pref);

                if(TextUtils.isEmpty(title))
                {
                    txtvTitulo.setError("Campo requerido");
                    return;
                }

                if(TextUtils.isEmpty(body))
                {
                    txtvCuerpo.setError("Campo requerido");
                    return;
                }

                createPost(new Post(uid,name,title,body,departamento,municipio,sector));

            }
        });
    }


    private void createPost(Post post){
        mDialog.setMessage("Enviando");
        mDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://8.9.11.231:3001/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);

        Call<Post> call = api.createPost(post.getUsuarioId(),
                                        post.getNombreUsuario(),
                                        post.getTitle(),
                                        post.getBody(),
                                        post.getDepartamentoId(),
                                        post.getMunicipioId(),
                                        post.getSectorId()
                                        );

        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                if(response.isSuccessful()){
                    if(response.body() != null){
                        Intent intent = new Intent(getApplicationContext(), PostActivity.class);
                        intent.putExtra("dep", response.body().getDepartamentoId());
                        intent.putExtra("mun", response.body().getMunicipioId());
                        intent.putExtra("sec", response.body().getSectorId());
                        startActivity(intent);
                    }
                }
                else
                {
                    Toast.makeText(getBaseContext(),"Hubo un problema", Toast.LENGTH_SHORT).show();
                }

                mDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                mDialog.dismiss();
            }
        });
    }
    private void bindUI()
    {
        txtvTitulo = findViewById(R.id.txtTitulo);
        txtvCuerpo = findViewById(R.id.txtCuerpo);
        spnSectores = findViewById(R.id.spnSectoresP);
        btnPublicar = findViewById(R.id.btnPublish);
    }


}
