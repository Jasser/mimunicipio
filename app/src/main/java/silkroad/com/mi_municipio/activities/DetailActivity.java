package silkroad.com.mi_municipio.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import silkroad.com.mi_municipio.fragments.DetailFragment;
import silkroad.com.mi_municipio.R;

public class DetailActivity extends AppCompatActivity
{

    private String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if(getIntent().getExtras() != null)
        {
            text = getIntent().getStringExtra("text");
        }

      //  DetailFragment detailsFragment = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.detailsFragment);
      //  detailsFragment.renderText(text);
    }
}
